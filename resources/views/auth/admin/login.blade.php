<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin Login - Dark Armor</title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Custom fonts for this template-->
  <link href="{{URL::asset('sb-admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{URL::asset('sb-admin/css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-8 col-lg-10 col-md-6">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Welcome, back!</h1>
                    <hr>
                    <img src="{{URL::asset('images/logo.png')}}" alt="" style="width:30%">
                    <hr>

                    <i>"Every time is money"</i> ~ Anonymous
                    <hr>
                  </div>

                  <div class="col-md-12">@include('layouts.errors-and-messages')</div>
                  <div class="col-md-12 col-md-offset-12">
                    <form action="{{ route('admin.login') }}" method="post">
                      {{ csrf_field() }}
                      <div class="form-group has-feedback">
                          <input name="email" type="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                      </div>
                      <div class="form-group has-feedback">
                          <input name="password" type="password" class="form-control" placeholder="Password">
                          
                      </div>
                      <div class="row">
                          <div class="col-xs-8">

                          </div>
                          <!-- /.col -->
                          <div class="col-xs-4">
                              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                          </div>
                          <!-- /.col -->
                      </div>
                  </form>
                    <hr>

                  </div>

                  {{-- <hr> --}}
                  <div class="text-center">
                    <a class="small" href="{{route('password.request')}}">I forgot my password</a><br>
                    <a class="small" href="{{route('register')}}" class="text-center">No account? Register here.</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{URL::asset('sb-admin/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{URL::asset('sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{URL::asset('sb-admin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{URL::asset('sb-admin/js/sb-admin-2.min.js')}}"></script>

</body>

</html>