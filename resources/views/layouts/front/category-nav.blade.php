<ul class="list-unstyled list-inline nav navbar-nav">
    @foreach($categories as $category)
        <li>
            @if($category->children()->count() > 0)
                @include('layouts.front.category-sub', ['subs' => $category->children])
            @else
                <a @if(request()->segment(2) == $category->slug) class="active" @endif href="{{route('front.category.slug', $category->slug)}}" class="list-group-item list-group-item-action list-group-item-dark">{{$category->name}} </a>
            @endif
        </li>
    @endforeach
</ul>