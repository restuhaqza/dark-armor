<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        $breadcumb = [
            ["name" => "Dashboard", "url" => route("admin.dashboard"), "icon" => "fa fa-dashboard"],
            ["name" => "Home", "url" => route("admin.dashboard"), "icon" => "fa fa-home"],

        ];

        $products = DB::table('products')->count();
        $orders = DB::table('order_product')->count();
        $members = DB::table('customers')->count();

        $data = array(
            'count_products' => $products,
            'count_orders' => $orders,
            'count_members' => $members
        );
        populate_breadcumb($breadcumb);
        return view('admin.dashboard', $data);
    }
}
